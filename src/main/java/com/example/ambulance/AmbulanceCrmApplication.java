package com.example.ambulance;

import com.example.ambulance.helpers.ClassPathTldsLoader;
import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan({"com.example.ambulance.*"})
@EnableTransactionManagement
//@EntityScan("com/example/ambulance/entities")
//@EnableJpaRepositories("com.example.ambulance.repositories")
public class AmbulanceCrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmbulanceCrmApplication.class, args);
    }

    @Bean
    @ConditionalOnMissingBean(ClassPathTldsLoader.class)
    public ClassPathTldsLoader classPathTldsLoader(){
        return new ClassPathTldsLoader();
    }

}
