package com.example.ambulance.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 * Created by avispa on 1/6/2018.
 */
@Aspect
@Configuration
public class AspectLogger {
@Before("execution (* com.example.ambulance.controllers.*.*(..))")
public void logControllers(JoinPoint joinPoint) {
        Logger logger = LogManager.getLogger(joinPoint.getTarget().getClass());
        logger.debug(joinPoint.getSignature().getName() + " invoked");
        }

@Before("execution (* com.example.ambulance.services.impl.*.*(..))")
public void logServices(JoinPoint joinPoint) {
        Logger logger = LogManager.getLogger(joinPoint.getTarget().getClass());
        logger.debug(joinPoint.getSignature().getName() + " invoked");
        }
        }
