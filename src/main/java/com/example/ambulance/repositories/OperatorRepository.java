package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.Operator;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by avispa on 30/4/2018.
 */
public interface OperatorRepository extends CrudRepository<Operator, Long> {
}
