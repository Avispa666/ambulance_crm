package com.example.ambulance.repositories.impl;

import com.example.ambulance.model.entities.PersistentLogins;
import org.hibernate.Session;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 * Created by avispa on 2/5/2018.
 */
@Repository("PersistentTokenRepository")
@Transactional
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        PersistentLogins logins = new PersistentLogins();
        logins.setUsername(token.getUsername());
        logins.setSeries(token.getSeries());
        logins.setToken(token.getTokenValue());
        logins.setLastUsed(token.getDate());
        //---------------------
        Session session = em.unwrap(Session.class).getSessionFactory().openSession();
        session.beginTransaction();
        session.save(logins);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        Session session = em.unwrap(Session.class).getSessionFactory().openSession();
        session.beginTransaction();
        PersistentLogins logins = session.get(PersistentLogins.class, seriesId);
        session.getTransaction().commit();
        session.close();
        //---------------------
        if (logins != null) {
            return new PersistentRememberMeToken(logins.getUsername(),
                    logins.getSeries(), logins.getToken(),logins.getLastUsed());
        }

        return null;
    }

    @Override
    public void removeUserTokens(String username) {
        Session session = em.unwrap(Session.class).getSessionFactory().openSession();
        session.beginTransaction();
        session.createQuery("delete from PersistentLogins"
                + " where username=:userName")
                .setParameter("userName", username).executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        Session session=em.unwrap(Session.class).getSessionFactory().openSession();
        session.beginTransaction();
        PersistentLogins logins=session.get(PersistentLogins.class, series);
        logins.setToken(tokenValue);
        logins.setLastUsed(lastUsed);
        session.update(logins);
        session.getTransaction().commit();
        session.close();
    }
}
