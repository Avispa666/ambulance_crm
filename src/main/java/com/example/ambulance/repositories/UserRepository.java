package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by avispa on 2/5/2018.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    void deleteByUsername(String username);
}
