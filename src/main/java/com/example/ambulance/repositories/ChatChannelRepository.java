package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.ChatChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by avispa on 2/6/2018.
 */
public interface ChatChannelRepository extends JpaRepository<ChatChannel, Long> {
    @Query(value = "select * from channels c where c.id_user1 in (:id, :id1) and c.id_user2 in (:id, :id1)", nativeQuery = true)
    ChatChannel findByUserId(@Param("id") Long id, @Param("id1") Long id1);
}
