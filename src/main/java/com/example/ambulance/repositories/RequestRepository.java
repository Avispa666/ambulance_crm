package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.Request;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by avispa on 30/4/2018.
 */
public interface RequestRepository extends JpaRepository<Request, Long> {

}
