package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by avispa on 25/5/2018.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);

}
