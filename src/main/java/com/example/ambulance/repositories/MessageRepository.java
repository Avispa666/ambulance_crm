package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by avispa on 2/6/2018.
 */
public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> getMessagesByChannelIdOrderByTime(Long channelId);
}
