package com.example.ambulance.repositories;

import com.example.ambulance.model.entities.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by avispa on 26/5/2018.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
    Privilege findByName(String name);
}
