package com.example.ambulance.model;

/**
 * Created by avispa on 4/6/2018.
 */
public class RequestDto {
    private String id;
    private String address;
    private String description;
    private String priority;
    private String status;
    private String createTime;
    private String operatorFullName;

    public RequestDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOperatorFullName() {
        return operatorFullName;
    }

    public void setOperatorFullName(String operatorFullName) {
        this.operatorFullName = operatorFullName;
    }

    public RequestDto(String id, String address, String description, String priority, String status, String createTime, String operatorFullName) {
        this.id = id;
        this.address = address;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.createTime = createTime;
        this.operatorFullName = operatorFullName;
    }
}
