package com.example.ambulance.model;

/**
 * Created by avispa on 2/6/2018.
 */
public class ChannelDto {
    private String id;

    public ChannelDto() {
    }

    public ChannelDto(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
