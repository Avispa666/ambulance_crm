package com.example.ambulance.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.OffsetDateTime;

import static com.example.ambulance.model.entities.Request.Status.CLOSED;
import static com.example.ambulance.model.entities.Request.Status.IN_PROGRESS;

/**
 * Created by avispa on 29/4/2018.
 */
@Entity
@Table(name = "requests")
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "operatorId")
    private Operator operator;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "priority", nullable = false)
    private int priority;

    @Column(name = "create_time", nullable = false)
    private OffsetDateTime createTime;

    public enum Status {
        NEW, IN_PROGRESS, CLOSED
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Type( type = "pgsql_enum" )
    private Status status;

    public Request() {
        status = Status.NEW;
        createTime = OffsetDateTime.now();
    }

    public void _nextStatus() {
        switch (status) {
            case NEW:
                status = IN_PROGRESS;
                break;
            case IN_PROGRESS:
                status = CLOSED;
                break;
            case CLOSED:
//                throw new RuntimeException("nextStatus called on closed request");
        }
    }

    public Status getStatus() {
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OffsetDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(OffsetDateTime createTime) {
        this.createTime = createTime;
    }
}
