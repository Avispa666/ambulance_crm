package com.example.ambulance.model.entities;

import javax.persistence.*;

/**
 * Created by avispa on 30/4/2018.
 */
@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
}
