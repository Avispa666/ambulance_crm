package com.example.ambulance.model;

/**
 * Created by avispa on 2/6/2018.
 */
public class MessageDto {
    private String content;
    private String senderId;
    private String receiverId;
    private String channelId;

    public MessageDto() {
    }

    public MessageDto(String content, String senderId, String receiverId, String channelId) {
        this.content = content;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.channelId = channelId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getContent() {
        return content;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getChannelId() {
        return channelId;
    }
}
