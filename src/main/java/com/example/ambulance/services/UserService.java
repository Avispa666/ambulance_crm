package com.example.ambulance.services;

import com.example.ambulance.model.UserDto;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.model.RegistrationDto;

import java.util.List;

/**
 * Created by avispa on 25/5/2018.
 */
public interface UserService {
    void save(RegistrationDto user);
    User findByUsername(String username);

    List<User> getAll();

    void deleteByUsername(String username);

    void save(User user);

    void changePassword(String username, String newPassword);

    UserDto findDtoByUsername(String username);

    List<UserDto> getAllDto();

    List<UserDto> search(String username);
}