package com.example.ambulance.services;

/**
 * Created by avispa on 26/5/2018.
 */
public interface SecurityService {
    String findLoggedInUsername();
    void autologin(String username, String password);
}
