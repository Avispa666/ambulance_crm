package com.example.ambulance.services;

import com.example.ambulance.model.entities.Operator;

import java.util.List;

/**
 * Created by avispa on 27/5/2018.
 */
public interface OperatorService {
    List<Operator> getAll();
    Operator getById(Long id);

    void deleteById(long id);
}
