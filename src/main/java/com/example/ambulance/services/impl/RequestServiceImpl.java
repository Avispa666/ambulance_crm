package com.example.ambulance.services.impl;

import com.example.ambulance.model.RequestDto;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.repositories.OperatorRepository;
import com.example.ambulance.repositories.UserRepository;
import com.example.ambulance.services.RequestService;
import com.example.ambulance.model.entities.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.ambulance.repositories.RequestRepository;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by avispa on 1/5/2018.
 */
@Service
public class RequestServiceImpl implements RequestService{

    private RequestRepository requestRepository;
    private UserRepository userRepository;
    private OperatorRepository operatorRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository, UserRepository userRepository, OperatorRepository operatorRepository) {
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
        this.operatorRepository = operatorRepository;
    }

    @Override
    public List<Request> getAllRequests() {
        List<Request> list =  requestRepository.findAll();
        list.sort((r1, r2) -> {
            if (r1.getPriority() > r2.getPriority()) return 1;
            if (r1.getPriority() == r2.getPriority()) return r1.getCreateTime().compareTo(r2.getCreateTime());
            return -1;
        });
        return list;
    }

    @Override
    public Request getById(Long id) {
        return requestRepository.getOne(id);
    }

    @Override
    public void nextStatus(Request request) {
        request._nextStatus();
        requestRepository.saveAndFlush(request);
    }

    @Override
    public void deleteById(Long id) {
        requestRepository.deleteById(id);
    }

    @Override
    public void save(Request request, String username) {
        User u = userRepository.findByUsername(username);
        request.setOperator(operatorRepository.findById(u.getId()).get());
        requestRepository.saveAndFlush(request);
    }

    @Override
    public void save(Request request) {
        requestRepository.saveAndFlush(request);
    }

    @Override
    public List<Request> getAllWithStatus(Request.Status status) {
        return getAllRequests().stream().filter(request -> request.getStatus() == status).collect(Collectors.toList());
    }

    @Override
    public RequestDto getDtoById(Long id) {
        return createDto(getById(id));
    }

    @Override
    public List<RequestDto> getAllRequestsDto() {
        return getAllRequests().stream().map(this::createDto).collect(Collectors.toList());
    }

    @Override
    public List<RequestDto> getAllWithStatusDto(Request.Status status) {
        return getAllWithStatus(status).stream().map(this::createDto).collect(Collectors.toList());
    }

    private RequestDto createDto(Request request) {
        if (request == null) return null;
        return new RequestDto(request.getId().toString(), request.getAddress(), request.getDescription(), request.getPriority() + "",
                request.getStatus().toString(), request.getCreateTime().format(DateTimeFormatter.ISO_DATE_TIME), request.getOperator().getUser().getFirstName() + " " +
                request.getOperator().getUser().getLastName());
    }
}
