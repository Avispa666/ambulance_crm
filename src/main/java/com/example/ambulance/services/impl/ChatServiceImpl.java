package com.example.ambulance.services.impl;

import com.example.ambulance.model.ChannelDto;
import com.example.ambulance.model.MessageDto;
import com.example.ambulance.model.entities.ChatChannel;
import com.example.ambulance.model.entities.Message;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.repositories.ChatChannelRepository;
import com.example.ambulance.repositories.MessageRepository;
import com.example.ambulance.repositories.UserRepository;
import com.example.ambulance.services.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by avispa on 2/6/2018.
 */
@Service
public class ChatServiceImpl implements ChatService {
    private MessageRepository messageRepository;
    private ChatChannelRepository chatChannelRepository;
    private UserRepository userRepository;


    @Autowired
    public ChatServiceImpl(MessageRepository messageRepository, ChatChannelRepository chatChannelRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.chatChannelRepository = chatChannelRepository;
        this.userRepository = userRepository;
    }


    @Override
    public List<MessageDto> getMessages(Long channelId, String username) {
        ChatChannel channel = chatChannelRepository.getOne(channelId);
        if (channel == null) return null;
        if (channel.getUser1().getUsername().equals(username) || channel.getUser2().getUsername().equals(username))
            return messageRepository.getMessagesByChannelIdOrderByTime(channelId).stream()
                    .map(this::createDto).collect(Collectors.toList());
        return null;
    }

    private MessageDto createDto(Message message) {
        return new MessageDto(message.getContent(), message.getSender().getId().toString(),
                (message.getChannel().getUser1().getId().equals(message.getSender().getId()) ? message.getChannel().getUser2().getId().toString()
                        : message.getChannel().getUser1().getId().toString())
                , message.getChannel().getId().toString());
    }

    @Override
    public void processMessage(MessageDto message) {
        Message m = new Message();
        m.setChannel(chatChannelRepository.getOne(Long.parseLong(message.getChannelId())));
        m.setContent(message.getContent());
        m.setTime(OffsetDateTime.now());
        m.setSender(userRepository.findById(Long.parseLong(message.getSenderId())).get());
        messageRepository.saveAndFlush(m);
    }

    @Override
    public ChannelDto establishChatSession(String receiverId, String senderUsername) {
        User receiver = userRepository.findById(Long.parseLong(receiverId)).get();
        User sender = userRepository.findByUsername(senderUsername);
        if (sender.equals(receiver)) return null;
        ChatChannel channel = chatChannelRepository.findByUserId(receiver.getId(), sender.getId());
        if (channel == null) {
            channel = new ChatChannel();
            channel.setUser1(receiver);
            channel.setUser2(sender);
            chatChannelRepository.saveAndFlush(channel);
        }
        return new ChannelDto(channel.getId().toString());
    }

    @Override
    public boolean isUserExists(String user) {
        return userRepository.findByUsername(user) != null;
    }
}
