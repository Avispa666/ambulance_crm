package com.example.ambulance.services.impl;

import com.example.ambulance.model.entities.Operator;
import com.example.ambulance.repositories.OperatorRepository;
import com.example.ambulance.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 27/5/2018.
 */
@Service
public class OperatorServiceImpl implements OperatorService {
    OperatorRepository operatorRepository;

    @Autowired
    public OperatorServiceImpl(OperatorRepository operatorRepository) {
        this.operatorRepository = operatorRepository;
    }

    @Override
    public List<Operator> getAll() {
        List<Operator> list = new ArrayList<>(10);
        operatorRepository.findAll().forEach(list::add);
        return list;
    }

    @Override
    public Operator getById(Long id) {
        return operatorRepository.findById(id).get();
    }

    @Override
    public void deleteById(long id) {
        operatorRepository.deleteById(id);
    }
}
