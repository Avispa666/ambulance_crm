package com.example.ambulance.services.impl;

import com.example.ambulance.model.UserDto;
import com.example.ambulance.model.entities.Role;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.model.RegistrationDto;
import com.example.ambulance.repositories.RoleRepository;
import com.example.ambulance.repositories.UserRepository;
import com.example.ambulance.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by avispa on 25/5/2018.
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LogManager.getLogger(UserServiceImpl.class);
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void save(RegistrationDto user) {
        User u = new User();
        u.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        u.setUsername(user.getUsername());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setEnabled(true);
        Role r = roleRepository.findByName("USER");
        if (r == null) {
            log.debug("role USER not found, created");
            r = new Role();
            r.setName("USER");
            roleRepository.saveAndFlush(r);
        }
        u.setRoles(Arrays.asList(r));
        log.debug("roles " + u.getRoles().size());

        userRepository.save(u);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>(10);
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public void deleteByUsername(String username) {
        userRepository.deleteByUsername(username);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void changePassword(String username, String newPassword) {
        User u = userRepository.findByUsername(username);
        u.setPassword(newPassword);
        userRepository.save(u);
    }

    @Override
    public UserDto findDtoByUsername(String username) {
        return createDto(findByUsername(username));
    }

    @Override
    public List<UserDto> getAllDto() {
        return getAll().stream().map(this::createDto).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> search(String username) {
        return getAllDto().stream().filter(userDto -> userDto.getUsername().matches(".*" + username + ".*"))
                .collect(Collectors.toList());
    }

    private UserDto createDto(User u) {
        if (u == null) return null;
        return new UserDto(u.getId().toString(), u.getUsername(), u.getFirstName(), u.getLastName(), u.getEnabled() + "");
    }
}
