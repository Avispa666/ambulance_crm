package com.example.ambulance.services;

import com.example.ambulance.model.ChannelDto;
import com.example.ambulance.model.MessageDto;
import com.example.ambulance.model.entities.Message;

import java.util.List;

/**
 * Created by avispa on 2/6/2018.
 */
public interface ChatService {
    List<MessageDto> getMessages(Long channelId, String username);

    void processMessage(MessageDto message);

    ChannelDto establishChatSession(String receiverId, String senderUsername);

    boolean isUserExists(String user);
}
