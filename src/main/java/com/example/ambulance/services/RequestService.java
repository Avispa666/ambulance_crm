package com.example.ambulance.services;

import com.example.ambulance.model.RequestDto;
import com.example.ambulance.model.entities.Request;

import java.util.List;

/**
 * Created by avispa on 1/5/2018.
 */
public interface RequestService {
    List<Request> getAllRequests();
    Request getById(Long id);
    void nextStatus(Request request);

    void deleteById(Long id);

    void save(Request request, String username);
    void save(Request request);

    List<Request> getAllWithStatus(Request.Status status);

    RequestDto getDtoById(Long id);

    List<RequestDto> getAllRequestsDto();

    List<RequestDto> getAllWithStatusDto(Request.Status status);
}
