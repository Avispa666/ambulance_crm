package com.example.ambulance.controllers.rest;

import com.example.ambulance.model.entities.Operator;
import com.example.ambulance.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by avispa on 1/6/2018.
 */
@RestController
@RequestMapping("/operators")
public class OperatorRestController {
    OperatorService operatorService;

    @Autowired
    public OperatorRestController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

//    @RequestMapping(method = RequestMethod.GET)
//    public List<Operator> getAll() {
//        return operatorService.getAll();
//    }

    @RequestMapping(value = "{id:[0-9]+}", method = RequestMethod.GET)
    public Operator getOperatorById(@PathVariable("id") String id) {
        return operatorService.getById(Long.parseLong(id));

    }

    @RequestMapping(value = "{id:[0-9]+}", method = RequestMethod.DELETE)
    public String deleteOperatorById(@PathVariable("id") String id) {
        operatorService.deleteById(Long.parseLong(id));
        return "operators";
    }
}
