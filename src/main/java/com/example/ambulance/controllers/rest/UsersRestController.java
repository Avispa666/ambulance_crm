package com.example.ambulance.controllers.rest;

import com.example.ambulance.model.UserDto;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by avispa on 2/6/2018.
 */
@RestController
@RequestMapping("/users")
public class UsersRestController {
    private static final Logger log = LogManager.getLogger(UsersRestController.class);
    UserService userService;

    @Autowired
    public UsersRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserDto> getAll(@RequestParam(value = "username", required = false) String username) {
        if (username == null || username.equals("")) return userService.getAllDto();
        return userService.search(username);
    }

    @RequestMapping(value = "{username}", method = RequestMethod.GET)
    public UserDto getUserById(@PathVariable("username") String username) {
        log.debug("getting user "+ username);
        return userService.findDtoByUsername(username);
    }

    @RequestMapping(value = "{username}", method = RequestMethod.DELETE)
    public void deleteUserById(@PathVariable("username") String username) {
        log.debug("trying to delete user " + username);
//        userService.deleteByUsername(username);
    }

    @RequestMapping(value = "{username}", method = RequestMethod.PUT)
    public void updateUserByUsername(@PathVariable("username") String username,
                                     @RequestBody UserDto userDto) {
        User u = userService.findByUsername(username);
        if (u == null) return;
        if (!userDto.getUsername().trim().equals("") && userService.findByUsername(userDto.getUsername().trim()) == null)
            u.setUsername(userDto.getUsername());
        if (!userDto.getFirstName().trim().equals("")) u.setFirstName(userDto.getFirstName().trim());
        if (!userDto.getLastName().trim().equals(""))
        u.setEnabled(Boolean.parseBoolean(userDto.getEnabled().trim()));
        userService.save(u);
    }
}
