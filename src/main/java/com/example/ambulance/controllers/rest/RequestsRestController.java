package com.example.ambulance.controllers.rest;

import com.example.ambulance.model.RequestDto;
import com.example.ambulance.model.entities.Request;
import com.example.ambulance.services.RequestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by avispa on 1/6/2018.
 */
@RestController
@RequestMapping("/requests")
public class RequestsRestController {
    private static final Logger log = LogManager.getLogger(RequestsRestController.class);
    private RequestService requestService;

    @Autowired
    public RequestsRestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @RequestMapping(value = "{id:[0-9]+}", method = RequestMethod.DELETE)
    public void deleteRequestById(@PathVariable("id") String id) {
        log.debug("trying to delete request " + id);
//        requestService.deleteById(Long.parseLong(id));
    }

    @RequestMapping(value = "{id:[0-9]+}", method = RequestMethod.PUT)
    public void updateRequestById(@PathVariable("id") String id,
                                  @RequestBody RequestDto request) {
        log.debug("updated request " + id);
        Request r = requestService.getById(Long.parseLong(id));
        if (!request.getAddress().equals("")) r.setAddress(request.getAddress());
        if (!request.getDescription().equals("")) r.setDescription(request.getDescription());
        if (!request.getPriority().equals("")) r.setPriority(Integer.parseInt(request.getPriority()));
        if (request.getStatus().equals("next")) r._nextStatus();
        requestService.save(r);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void createRequest(@RequestBody RequestDto request, Principal principal) {
        log.debug("creating request");

        if (principal == null) return;
        log.debug("principal " + principal.getName());
        Request r = new Request();
        r.setAddress(request.getAddress());
        r.setDescription(request.getDescription());
        r.setPriority(Integer.parseInt(request.getPriority()));
        requestService.save(r, principal.getName());
    }

    @RequestMapping(value = "{id:[0-9]+}", method = RequestMethod.GET)
    public RequestDto getRequestById(@PathVariable("id") String id) {
        log.debug("get request " + id);
        return requestService.getDtoById(Long.parseLong(id));
    }

    @RequestMapping(value = "/all/{status_id:[0-9]+}", method = RequestMethod.GET)
    public List<RequestDto> getByStatus(@PathVariable(value = "status_id") String status_id) {
        switch (status_id) {
            case "1":
                return requestService.getAllRequestsDto();
            case "2":
                return requestService.getAllWithStatusDto(Request.Status.NEW);
            case "3":
                return requestService.getAllWithStatusDto(Request.Status.IN_PROGRESS);
            case "4":
                return requestService.getAllWithStatusDto(Request.Status.CLOSED);
            default:
                return null;
        }
    }
}
