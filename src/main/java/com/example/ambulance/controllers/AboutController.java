package com.example.ambulance.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by avispa on 1/6/2018.
 */
@Controller
@RequestMapping("/about")
public class AboutController {

    public String about() {
        return "about";
    }
}
