package com.example.ambulance.controllers;

import com.example.ambulance.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by avispa on 27/5/2018.
 */
@Controller
@RequestMapping("/operators")
public class OperatorController {
    OperatorService operatorService;

    @Autowired
    public OperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getOperators(Model model) {
        model.addAttribute("operators", operatorService.getAll());
        return "operators";
    }
}
