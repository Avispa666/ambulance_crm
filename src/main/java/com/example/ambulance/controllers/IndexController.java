package com.example.ambulance.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by avispa on 28/4/2018.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    private static final Logger log = LogManager.getLogger(IndexController.class);

    public String index(Model model, Principal principal) {
        model.addAttribute("name", "Ambulance CRM");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Set<String> roles = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        roles.forEach(log::debug);
        log.debug("principal " + (principal == null ? "null" : principal.getName()));
        return "index";
    }
}
