package com.example.ambulance.controllers;

import com.example.ambulance.model.RegistrationDto;
import com.example.ambulance.services.SecurityService;
import com.example.ambulance.services.UserService;
import com.example.ambulance.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by avispa on 25/5/2018.
 */
@Controller
public class RegistrationController {
    private UserService userService;
    private SecurityService securityService;
    private UserValidator userValidator;

    @Autowired
    public RegistrationController(UserService s, SecurityService securityService,
                                  UserValidator userValidator) {
        this.userService = s;
        this.securityService = securityService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model, Principal principal) {
        if (principal != null) return "redirect:/";
        model.addAttribute("userDto", new RegistrationDto());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userDto") RegistrationDto userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPassword());

        return "redirect:/";
    }
}
