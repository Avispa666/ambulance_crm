package com.example.ambulance.controllers;

import com.example.ambulance.model.RequestDto;
import com.example.ambulance.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by avispa on 30/4/2018.
 */
@Controller
@RequestMapping("/requests")
public class RequestsController {
    private RequestService service;

    @Autowired
    public RequestsController(RequestService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String requests(Model model) {
        model.addAttribute("requests", service.getAllRequestsDto());
        return "requests";
    }

    @RequestMapping(value = "/edit/{id:[0-9]+}", method = RequestMethod.GET)
    public String edit(Model model, @PathVariable("id") String id) {
        RequestDto r = service.getDtoById(Long.parseLong(id));
        if (r == null) return "redirect:/requests";
        model.addAttribute("request", r);
        return "edit_request";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return "add_request";
    }
}
