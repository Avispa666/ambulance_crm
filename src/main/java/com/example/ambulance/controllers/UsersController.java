package com.example.ambulance.controllers;

import com.example.ambulance.model.UserDto;
import com.example.ambulance.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by avispa on 2/6/2018.
 */
@Controller
@RequestMapping("/users")
public class UsersController {
    UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String users(Model model) {
        model.addAttribute("users", userService.getAll());
        return "users";
    }

    @RequestMapping(value = "/info/{username}", method = RequestMethod.GET)
    public String userInfo(Model model, @RequestParam("username") String username) {
        UserDto user = userService.findDtoByUsername(username);
        if (user == null) return "redirect:/users";
        model.addAttribute("user", user);
        return "user_info";
    }

    @RequestMapping(value = "/edit/{username}", method = RequestMethod.GET)
    public String editUser(Model model, @RequestParam("username") String username) {
        UserDto userDto = userService.findDtoByUsername(username);
        if (userDto == null) return "redirect:/users";
        model.addAttribute("user", userDto);
        return "edit_user";
    }
}
