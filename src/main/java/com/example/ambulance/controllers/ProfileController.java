package com.example.ambulance.controllers;

import com.example.ambulance.model.entities.User;
import com.example.ambulance.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

/**
 * Created by avispa on 2/6/2018.
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {
    UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String profile(Model model, Principal principal) {
        if (principal == null) return "redirect:/";
        User user = userService.findByUsername(principal.getName());
        if (user == null) return "redirect:/";
        model.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping(method = RequestMethod.POST)
    public boolean changePassword(@RequestParam("username") String username,
                                  @RequestParam("old_password") String oldPassword,
                                  @RequestParam("new_password") String newPassword,
                                  @RequestParam("confirm_password") String confirmPassword) {
        if (newPassword.equals(confirmPassword)) userService.changePassword(username, newPassword);
        return true;
    }
}
