package com.example.ambulance.controllers;

import com.example.ambulance.model.ChannelDto;
import com.example.ambulance.model.MessageDto;
import com.example.ambulance.model.entities.User;
import com.example.ambulance.services.ChatService;
import com.example.ambulance.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by avispa on 2/6/2018.
 */
@Controller
public class ChatController {
    private static final Logger log = LogManager.getLogger(ChatController.class);
    private ChatService chatService;
    private UserService userService;

    @Autowired
    public ChatController(ChatService chatService, UserService userService) {
        this.chatService = chatService;
        this.userService = userService;
    }

    @MessageMapping("/chat.{channelId}")
    @SendTo("/topic/chat.{channelId}")
    public MessageDto sendMessage(@DestinationVariable String channelId, MessageDto message) {
        log.debug("processing message: " + message.getContent());
        chatService.processMessage(message);
        return message;
    }

    @RequestMapping(value = "/startchat", method = RequestMethod.POST)
    @ResponseBody
    public ChannelDto startChat(@RequestParam("receiverId") String receiverId, Principal principal) {
        return chatService.establishChatSession(receiverId, principal.getName());
    }

    @RequestMapping(value = "/chat", method = RequestMethod.GET) //TODO change to post
    public String getChatPage(@RequestParam("user") String user, Model model, Principal principal) {
        if (user.equals(principal.getName()) && !chatService.isUserExists(user)) return "redirect:/users";
        User sender = userService.findByUsername(principal.getName());
        User receiver = userService.findByUsername(user);
        model.addAttribute("sender", sender);
        if (sender.compareRoles(receiver) == -1) receiver.maskName();
        model.addAttribute("receiver", receiver);

        return "chatPage";
    }

    @RequestMapping(value = "/chat/{channelId:[0-9]+}", method = RequestMethod.GET)
    @ResponseBody
    public List<MessageDto> getMessages(@PathVariable("channelId") String channelId, Principal principal) {
        if (principal == null) return null;
        return chatService.getMessages(Long.parseLong(channelId), principal.getName());
    }
}
