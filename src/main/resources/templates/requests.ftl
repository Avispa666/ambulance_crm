<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
<head>
    <title>Ambulace CRM</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<@spring.url'/css/main.css'/>" />
</head>
<body class="landing">

<!-- Header -->
<header id="header" class="alt">
    <h1><a href="<@spring.url'/index.ftl'/>">Ambulance CRM</a></h1>
    <a href="#nav">Menu</a>
</header>

<!-- Nav -->
<#include "nav.ftl"/>

<!-- Banner -->
<section id="banner">
    <i class="icon fa-diamond"></i>
    <h2>Etiam adipiscing</h2>
    <p>Magna feugiat lorem dolor egestas</p>
    <ul class="actions">
        <li><a href="#" class="button big special">Learn More</a></li>
    </ul>
</section>

<!-- One -->
<section id="one" class="wrapper style1">
    <div class="inner" id="content-div">
        <h3>Requests</h3>
        <select id="selector" style="width: 20%; float: left">
            <option value="1">Any</option>
            <option value="2">New</option>
            <option value="3">In progress</option>
            <option value="4">Closed</option>
        </select>
        <button style="margin-top: 10px; margin-bottom: 10px" id="get-by-status-button">Search</button>
        <table id="requests-table">
            <tr><th>Address</th><th>Description</th><th>Operator</th><th>Priority</th><th>Creation time</th>
                <th>Status</th><th></th><th></th></tr>
            <#list requests as request>
            <tr class="request-row">
                <td>${request.address}</td><td>${request.description}</td>
                <td>${request.operatorFullName}</td>
                <td>${request.priority}</td><td>${request.createTime}</td><td>${request.status}</td>
                <td><a href="<@spring.url'/requests/edit/${request.id}'/>">
                    <button class="request-edit-buttons" id="edit-${request.id}">Edit</button>
                </td></a>
                <td>
                    <button class="request-delete-buttons" id="edit-${request.id}">Delete</button>
                </td>
            </tr>
            <#--${request.id} ${request.operatorId} ${request.address} ${request.description}-->
            <#--${request.priority} ${request.createTime} ${request.status}<br>-->
            </#list>
        </table>

    </div>
</section>

<!-- Two -->
<section id="two" class="wrapper special">
    <div class="inner">
        <header class="major narrow">
            <h2>Aliquam Blandit Mauris</h2>
            <p>Ipsum dolor tempus commodo turpis adipiscing Tempor placerat sed amet accumsan</p>
        </header>
        <div class="image-grid">
            <a href="#" class="image"><img src="<@spring.url'/images/pic03.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic04.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic05.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic06.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic07.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic08.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic09.jpg'/>" alt="" /></a>
            <a href="#" class="image"><img src="<@spring.url'/images/pic10.jpg'/>" alt="" /></a>
        </div>
        <ul class="actions">
            <li><a href="#" class="button big alt">Tempus Aliquam</a></li>
        </ul>
    </div>
</section>

<!-- Three -->
<section id="three" class="wrapper style3 special">
    <div class="inner">
        <header class="major narrow	">
            <h2>Magna sed consequat tempus</h2>
            <p>Ipsum dolor tempus commodo turpis adipiscing Tempor placerat sed amet accumsan</p>
        </header>
        <ul class="actions">
            <li><a href="#" class="button big alt">Magna feugiat</a></li>
        </ul>
    </div>
</section>

<!-- Four -->
<section id="four" class="wrapper style2 special">
    <div class="inner">
        <header class="major narrow">
            <h2>Get in touch</h2>
            <p>Ipsum dolor tempus commodo adipiscing</p>
        </header>
        <form action="#" method="POST">
            <div class="container 75%">
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input name="name" placeholder="Name" type="text" />
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input name="email" placeholder="Email" type="email" />
                    </div>
                    <div class="12u$">
                        <textarea name="message" placeholder="Message" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <ul class="actions">
                <li><input type="submit" class="special" value="Submit" /></li>
                <li><input type="reset" class="alt" value="Reset" /></li>
            </ul>
        </form>
    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="inner">
        <ul class="icons">
            <li><a href="#" class="icon fa-facebook">
                <span class="label">Facebook</span>
            </a></li>
            <li><a href="#" class="icon fa-twitter">
                <span class="label">Twitter</span>
            </a></li>
            <li><a href="#" class="icon fa-instagram">
                <span class="label">Instagram</span>
            </a></li>
            <li><a href="#" class="icon fa-linkedin">
                <span class="label">LinkedIn</span>
            </a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="<@spring.url'/js/jquery.min.js'/>"></script>
<script src="<@spring.url'/js/skel.min.js'/>"></script>
<script src="<@spring.url'/js/util.js'/>"></script>
<script src="<@spring.url'/js/requests.js'/>"></script>
<script src="<@spring.url'/js/main.js'/>"></script>
</body>
</html>