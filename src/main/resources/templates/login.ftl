<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Login</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<@spring.url'/css/main.css'/>" />
    <link rel="stylesheet" href="<@spring.url'/css/login.css'/>" />
</head>
<body style="background-color: #1E2832">
    <div class="container-login100">
        <div class="wrap-login100">
            <form action="<@spring.url'/login'/>" method="post" class="login100-form">
                <span class="login100-form-title">Login</span>
                <input type="text" style="margin-bottom: 16px" id="username" name="username" placeholder="Username">
                <input type="text" style="margin-bottom: 16px" id="password" name="password" placeholder="Password">
                <div class="contact100-form-checkbox">
                    <input type="checkbox" id="remember-me" name="remember-me">
                    <label for="remember-me" class="label-checkbox100"><span></span>Remember me</label>
                </div>
                <div class="container-login100-form-btn">
                    <input type="submit" class="special login100-form-btn" value="Login">
                </div>
                <div style="float:left; color: #ffffff">Don`t have an account?</div>
                <div style="float:right;"><a href="<@spring.url'/registration'/>">Register</a></div>
            </form>
        </div>
    </div>
</body>
</html>