<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
<head>
    <title>Ambulace CRM</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<form action="" method="POST">
    <#--<input type="text"-->
           <#--name="${spring.status.expression}"-->
           <#--value="${spring.status.value?html}"/><br>-->
    <label for="firstName">First name</label><@spring.formInput "userDto.firstName" ""/>
    <@spring.showErrors "<br>"/><br/>
    <label for="lastName">Last name</label><@spring.formInput "userDto.lastName"/>
    <@spring.showErrors "<br>"/><br/>
    <label for="username">Login</label><@spring.formInput "userDto.username"/>
    <@spring.showErrors "<br>"/><br/>
    <label for="password">Password</label><@spring.formInput "userDto.password"/>
    <@spring.showErrors "<br>"/><br/>
    <label for="matchingPassword">Confirm password</label><@spring.formInput "userDto.matchingPassword"/>
    <@spring.showErrors "<br>"/><br/>
    <input type="submit" value="Register!">
</form>
</body>
</html>