<#import "/spring.ftl" as spring/>
<#assign  security=JspTaglibs["http://www.springframework.org/security/tags"] />
<nav id="nav">
    <ul class="links">
        <li><a href="<@spring.url'/'/>">Home</a></li>
        <li><a href="<@spring.url'/about'/>">About</a></li>
    <@security.authorize access="isAuthenticated()">
        <li><a href="<@spring.url'/requests'/>">Requests</a></li>
        <li><a href="<@spring.url'/operators'/>">Operators</a></li>
        <li><a href="<@spring.url'/users'/>">Users</a></li>
        <li><a href="<@spring.url'/logout'/>">Sign Out</a></li>
    </@security.authorize>
    <@security.authorize access="! isAuthenticated()">
        <li><a href="<@spring.url'/login'/>">Sign In</a></li>
        <li><a href="<@spring.url'/registration'/>">Register</a></li>
    </@security.authorize>
    </ul>
</nav>