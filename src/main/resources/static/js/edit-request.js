$(document).ready(function () {
    $(".request-save-buttons").click(
        function (event) {
            var id = event.target.id.toString().substr(5);
            var nextStatus = $("#status").is(':checked');
            if (nextStatus) nextStatus = 'next';
            $.ajax({
                type: 'PUT',
                url: '/requests/' + id,
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify({
                    address: '' + $("#address").val(),
                    description: '' + $("#description").val(),
                    operatorFullName: '',
                    priority: '' + $("#priority").val(),
                    createTime: '',
                    status: nextStatus
                }),
                success: function () {
                    $("#saved").show();
                }
            });
        }
    )
});