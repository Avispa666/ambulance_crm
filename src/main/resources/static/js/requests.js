$(document).ready(function () {
    $(".request-delete-buttons").click(delete_request);
    $("#get-by-status-button").click(get_requests);
});

var get_requests = function (event) {
    var value = $("#selector").val();
    $.ajax({
        type: 'GET',
        url: '/requests/all/' + value,
        dataType: 'json',
        success: function (requests) {
            if (requests == null) return;
            $(".request-row").remove();
            requests.forEach(function (request) {
                $("#requests-table").append("<tr class=\"request-row\">" +
                "<td>" + request.address + "</td><td>" + request.description + "</td>" +
                "<td>" + request.operatorFullName + "</td>" +
                "<td>" + request.priority + "</td><td>" + request.createTime + "</td><td>" + request.status + "</td>" +
                "<td><a href=\"<@spring.url'/requests/edit/" + request.id + "'/>\">" +
                    "<button class=\"request-edit-buttons\" id=\"edit-" + request.id + "\">Edit</button>" +
                    "</td></a>" +
                    "<td>" +
                    "<button class=\"request-delete-buttons\" id=\"edit-" + request.id + "\">Delete</button>" +
                    "</td>" +
                    "</tr>");
            })
        }
    })
};

var delete_request = function (event) {
    var id = event.target.id.toString().substr(5);
    $.ajax({
        type: 'DELETE',
        url: '/requests/' + id,
        success: function () {

        }
    });
};