var chatWebSocket;
var channelId;
var user1fullName;
var senderId;
var user2fullName;
var receiverId;
var socket;

$(document).ready(function () {
    $("#send-button").click(function(e) {
        e.preventDefault();
        sendChatMessage();
    });
    senderId = $("#senderId-input").val();
    receiverId = $("#receiverId-input").val();
    connect();
    whenConnected(construct);
});

var connect = function () {
    console.log("connect");
    socket = Stomp.over(new SockJS('/ws'));
    socket.debug = null;
    socket.connect({}, onOpen, onClose);
};

var disconnect = function () {
    console.log("disconnect");
    socket.disconnect();
    socket = null;
};

var onOpen = function () {
};

var onClose = function () {
    alert('You have disconnected, hit "OK" to reload.');
    // console.log("onclose");
    window.location.reload();
};

var isConnected = function () {
    return (socket && socket.connected);
};

var whenConnected = function (_do) {
    setTimeout(
        function () {
            if (isConnected()) {
                if (_do !== null) {
                    _do();
                }
                return;
            } else {
                whenConnected(_do);
            }
        }, 1000);

};

var establishChatSession = function (onSuccess) {
    $.ajax({
        type: 'POST',
        url: '/startchat',
        data: {
            receiverId: receiverId
        },
        dataType: 'json',
        success: function (channelDto) {
            onSuccess(channelDto);
        }
    });
};

var getMessages = function (channelId, onSuccess) {
    $.ajax({
        type: 'GET',
        url: '/chat/' + channelId,
        dataType: 'json',
        success: function (messages) {
            onSuccess(messages);
        }
    });
};

var construct = function () {
    establishChatSession(establishChannel);
};

var onMessage = function (response) {
    console.log("message arrived");
    addToUI(JSON.parse(response.body));
    scrollToLatestMessage();
};

var establishChannel = function (channelDto) {
    chatWebSocket = socket;
    channelId = channelDto.id;
    chatWebSocket.subscribe('/topic/chat.' + channelId, function (response) {
        console.log("callback");
        onMessage(response);
    });
    getExistingChatMessages();
};

var getExistingChatMessages = function () {
    getMessages(channelId, function (messages) {
        messages.forEach(function (message) {
            // console.log(message.content);
            addToUI(message);
        });
        scrollToLatestMessage();
    });
};

var scrollToLatestMessage = function () {
    //TODO scroll to latest message
};

var addToUI = function (message) {
    //TODO add message to ui
    var align;
    if (message.senderId == senderId) align = "right";
    else align = "left";
    $("#messages").append("<p align='" + align + "' style='background-color: cyan; border-radius: 15px; padding: 10px' >"
        + message.content + "</p>");
};

var sendChatMessage = function () {
    var message = $("#message-input").val().toString().trim();
    if (!message || message === '') return;
    chatWebSocket.send("/app/chat." + channelId, {}, JSON.stringify({
        content: message,
        senderId: senderId,
        receiverId: receiverId,
        channelId: channelId
    }));
    $("#message-input").val("");
};