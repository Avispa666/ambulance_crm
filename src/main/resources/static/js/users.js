$(document).ready(function () {
   $("#get-by-username-button").click(get_users);

});

var get_users = function () {
    $.ajax({
        type: 'GET',
        url: '/users/all' ,
        data: {
            username: $("#input-username").val()
        },
        dataType: 'json',
        success: function (users) {
            $(".user-row").remove();
            if (users == null) return;
            users.forEach(function (user) {
                $("#users-table").append("<tr class=\"user-row\">" +
                "<td><a href=\"/users/info" + user.username + "\">" + user.username + "</a></td>" +
                    "<td>" + user.firstName + "</td><td>" + user.lastName + "</td><td>" + (user.enabled? "Yes" : "No") + "</td>" +
                    "</tr>");
            })
        }
    });
};