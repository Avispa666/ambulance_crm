package com.avispa.ambulancecrm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avispa.ambulancecrm.model.RequestDto;

import java.util.ArrayList;

/**
 * Created by avispa on 6/6/2018.
 */

public class RequestAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<RequestDto> requests;

    public RequestAdapter(Context context, ArrayList<RequestDto> requests) {
        this.context = context;
        this.requests = requests;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int position) {
        return requests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) v = layoutInflater.inflate(R.layout.list_item, parent, false);
//        v.setMinimumHeight(100);
        RequestDto r = requests.get(position);
        ((TextView) v.findViewById(R.id.item_address)).setText(r.getAddress());
        ((TextView) v.findViewById(R.id.item_description)).setText(r.getDescription());
        ((TextView) v.findViewById(R.id.item_priority)).setText(r.getPriority());
        ((TextView) v.findViewById(R.id.item_status)).setText(r.getStatus());
        return v;
    }
}
