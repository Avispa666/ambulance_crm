package com.avispa.ambulancecrm.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by avispa on 5/6/2018.
 */

public interface AmbulanceCrmApi {
    @GET("/requests/all/{status_id}")
    Call<List<RequestDto>> getRequests(@Path("status_id") String statusId);

    @POST("/requests/")
    Call<Void> addRequest(@Body RequestDto requestDto);

    @DELETE("/requests/{id}")
    Call<Void> deleteRequest(@Path("id") String id);

    @PUT("/requests/{id}")
    Call<Void> updateRequest(@Path("id") String id, @Body RequestDto requestDto);
}
