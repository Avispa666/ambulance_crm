package com.avispa.ambulancecrm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.avispa.ambulancecrm.model.RequestDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestInfoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Spinner spinner;
    private CheckBox checkBox;
    private RequestDto request;
    private TextView descriptionTv;
    private TextView addressTv;
    private EditText descriptionEt;
    private EditText addressEt;
    private ViewSwitcher addressSwitcher;
    private ViewSwitcher descriptionSwitcher;
    private boolean editing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            request = (new ObjectMapper()).readValue(this.getIntent().getStringExtra("request"),RequestDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        spinner = (Spinner) findViewById(R.id.info_priority_spinner);
        ((TextView) findViewById(R.id.info_status_tv)).setText("Status: " + request.getStatus());
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        descriptionTv = (TextView) findViewById(R.id.info_description_tv);
        descriptionTv.setText(request.getDescription());
        descriptionEt = (EditText) findViewById(R.id.info_description_field);
        descriptionEt.setText(request.getDescription());
        addressSwitcher = (ViewSwitcher) findViewById(R.id.address_switcher);
        descriptionSwitcher = (ViewSwitcher) findViewById(R.id.description_switcher);
        addressTv = (TextView) findViewById(R.id.info_address_tv);
        addressTv.setText(request.getAddress());
        addressEt = (EditText) findViewById(R.id.info_address_field);
        addressEt.setText(request.getAddress());
        String[] data = {"1", "2", "3", "4", "5"};
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data));
        spinner.setSelection(Integer.parseInt(request.getPriority()) - 1);
        spinner.setEnabled(false);
        spinner.setClickable(false);
        checkBox.setEnabled(false);


        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_info);
        fab.setOnClickListener(view -> {
            if (!editing) {
                spinner.setEnabled(true);
                spinner.setClickable(true);
                checkBox.setEnabled(true);
                addressSwitcher.showNext();
                descriptionSwitcher.showNext();
                fab.setImageResource(android.R.drawable.ic_menu_save);
                editing = true;
            } else {
                request.setDescription(descriptionEt.getText().toString().trim());
                request.setAddress(addressEt.getText().toString().trim());
                request.setPriority(spinner.getSelectedItem().toString());
                if (checkBox.isChecked()) request.setStatus("next");
                sendToServer(request);
                fab.setImageResource(android.R.drawable.ic_menu_edit);
            }
        });
        Drawable myFabSrc = getResources().getDrawable(android.R.drawable.ic_input_add);
        Drawable willBeWhite = myFabSrc.getConstantState().newDrawable();
        willBeWhite.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        fab.setImageDrawable(willBeWhite);

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
    }

    private void sendToServer(RequestDto request) {
        App.getApi().updateRequest(request.getId(), request).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("TAG", "onResponse: response code " + response.code());
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(RequestInfoActivity.this, "Failed to edit request", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            super.onBackPressed();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.request_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
