package com.avispa.ambulancecrm;

import android.app.Application;
import android.content.SharedPreferences;

import com.avispa.ambulancecrm.model.AmbulanceCrmApi;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by avispa on 5/6/2018.
 */

public class App extends Application {
    private static AmbulanceCrmApi api;
    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static AmbulanceCrmApi getApi() {
        return api;
    }

    public static void build(String credentials) {
        String[] t = credentials.split(" ");
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder = originalRequest.newBuilder().header("Authorization", Credentials.basic(t[0], t[1]));
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://avispa.asuscomm.com:5554")
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        api = retrofit.create(AmbulanceCrmApi.class);
    }
}
