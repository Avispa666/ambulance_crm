package com.avispa.ambulancecrm.model;

/**
 * Created by avispa on 5/6/2018.
 */

public class RequestDto {
    private String id;
    private String description;
    private String address;
    private String priority;
    private String status;
    private String createTime;
    private String operatorFullName;

    public RequestDto(String description, String address, String priority) {
        this.id = "";
        this.description = description;
        this.status = "NEW";
        this.createTime = "";
        this.operatorFullName ="";
        this.address = address;
        this.priority = priority;
    }

    public RequestDto() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOperatorFullName() {
        return operatorFullName;
    }

    public void setOperatorFullName(String operatorFullName) {
        this.operatorFullName = operatorFullName;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
