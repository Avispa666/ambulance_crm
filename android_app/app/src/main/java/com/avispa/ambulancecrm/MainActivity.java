package com.avispa.ambulancecrm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.avispa.ambulancecrm.model.RequestDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {
    private static final String LOG_TAG = "my_log";
    private static final int REQUEST_CODE_ADD = 1;
    private static final int REQUEST_CODE_INFO = 2;
    private ArrayList<RequestDto> list;
    private ListView lv;
    private Spinner statusSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ObjectMapper mapper = new ObjectMapper();
        statusSpinner = (Spinner) findViewById(R.id.status_spinner);
        String[] data = {"Any", "New", "In progress", "Closed"};
        statusSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data));
        statusSpinner.setOnItemSelectedListener(this);
        lv = (ListView) findViewById(R.id.requests);
        list = new ArrayList<>();
        lv.setAdapter(new RequestAdapter(MainActivity.this, list));
        registerForContextMenu(lv);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this, RequestInfoActivity.class);
            try {
                intent.putExtra("request", mapper.writeValueAsString(list.get(position)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            MainActivity.this.startActivityForResult(intent, REQUEST_CODE_INFO);
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_main);
        fab.setOnClickListener(view ->
                MainActivity.this.startActivityForResult(new Intent(MainActivity.this, AddActivity.class), REQUEST_CODE_ADD));

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        updateListView();
    }

    private void updateListView() {
        if (App.getApi() == null) return;
        Log.d("TAG", "updateListView: " + (1 + statusSpinner.getSelectedItemPosition()));
        App.getApi().getRequests("" + (statusSpinner.getSelectedItemPosition() + 1)).enqueue(new Callback<List<RequestDto>>() {
            @Override
            public void onResponse(Call<List<RequestDto>> call, Response<List<RequestDto>> response) {
                Log.d("TAG", "onResponse: getting a response");
                if (response.body() == null) return;
                Log.d("TAG", "onResponse: adding requests");
                list.ensureCapacity(response.body().size());
                list.clear();
                list.addAll(response.body());
                ((BaseAdapter) lv.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<RequestDto>> call, Throwable t) {
                Log.e("TAG", "onFailure: failure to fetch requests " + t.getMessage(), t);
                updateListView();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADD:
            case REQUEST_CODE_INFO:
                updateListView();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
        finishAndRemoveTask();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.logout).setVisible(
                getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE).getString("credentials",null) != null
        );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            case R.id.refresh_item:
                updateListView();
                return true;
            case R.id.logout:
                SharedPreferences.Editor e =
                        getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE).edit();
                e.remove("credentials");
                e.commit();
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.requests) {
            getMenuInflater().inflate(R.menu.request_list_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//        Toast.makeText(MainActivity.this, "menu item " + info.position, Toast.LENGTH_LONG).show();
        Log.d("TAG", "onContextItemSelected: menu item" + info.position);
        switch (item.getItemId()) {
            case R.id.delete_item:
                deleteRequest(list.get(info.position).getId());
                return true;
            case R.id.refresh_item:
                updateListView();
                return true;
            case R.id.next_status_item:
                setNextStatus(list.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void setNextStatus(RequestDto r) {
        App.getApi().updateRequest(r.getId(), r).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                updateListView();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Connection to server failed", Toast.LENGTH_SHORT).show();
                Log.e("TAG", "onFailure: " + t.getMessage(), t);
            }
        });
    }

    private void deleteRequest(String id) {
        App.getApi().deleteRequest(id).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                updateListView();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Connection to server failed", Toast.LENGTH_SHORT).show();
                Log.e("TAG", "onFailure: " + t.getMessage(), t);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        updateListView();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
