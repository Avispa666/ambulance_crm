package com.avispa.ambulancecrm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.avispa.ambulancecrm.model.RequestDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Spinner prioritySpinner = (Spinner) findViewById(R.id.priority_spinner);
        String[] data = {"1", "2", "3", "4", "5"};
        prioritySpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView addressField = (TextView) findViewById(R.id.address_field);
                TextView descriptionField = (TextView) findViewById(R.id.description_field);
                RequestDto request = new RequestDto(descriptionField.getText().toString().trim(),
                        addressField.getText().toString().trim(), prioritySpinner.getSelectedItem().toString());
                try {
                    App.getApi().addRequest(request).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Log.d("TAG", "response code " +response.code());
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Log.e("TAG", "onFailure: " + t.getMessage(), t);
                            Toast.makeText(AddActivity.this, "Oops! " + t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class SendData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String data = "";

//            HttpURLConnection httpURLConnection = null;
            try {
//                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
//                httpURLConnection.setRequestMethod("POST");
//                httpURLConnection.setRequestProperty("Content-Type", "application/json");
//                httpURLConnection.setDoOutput(true);
//
//                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
//                wr.writeBytes("PostData=" + params[1]);
//                wr.flush();
//                wr.close();
//                Log.d("TAG", "doInBackground: http code "+ httpURLConnection.getResponseCode());
//                InputStream in = httpURLConnection.getInputStream();
//                InputStreamReader inputStreamReader = new InputStreamReader(in);
//
//                int inputStreamData = inputStreamReader.read();
//                while (inputStreamData != -1) {
//                    char current = (char) inputStreamData;
//                    inputStreamData = inputStreamReader.read();
//                    data += current;
//                }
//                HttpPost
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
//                if (httpURLConnection != null) {
//                    httpURLConnection.disconnect();
//                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("TAG", result); // this is expecting a response code to be sent from your server upon receiving the POST data
            Intent intent = new Intent();
            intent.putExtra("name", result);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

}
